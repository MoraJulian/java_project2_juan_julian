package spellingbee.server;

public class Utility {
	/**
	 * Looks if all the letters in first String (lettersToValidate) are found in lettersToLook. If they
	 * are all found, then it returns true. If not, returns false.
	 * @author Juan-Carlos Sreng-Flores
	 * @param lettersToValidate
	 * @param lettersToLook
	 * @return boolean type, returns whether a letter is found in the letter combination
	 */
	public static boolean containsAllLetters(String lettersToValidate , String lettersToLook) {
		boolean contains = false;
		for(int i = 0; i<lettersToValidate.length(); i++) {
			contains = false;
			for(int j = 0; j<lettersToLook.length(); j++) {
				if(lettersToValidate.charAt(i) == lettersToLook.charAt(j)) {
					contains = true;
					break;
				}
			}
			if(!contains) {
				break;
			}
		}
		return contains;
	}
}
