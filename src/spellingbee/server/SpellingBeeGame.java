package spellingbee.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**A Class that implements the ISpellingBeeGame interface. The Class constructs/mutates,maintains all the relevant game data needed
 * to play the Spelling Bee game. 
 * @author Julian Mora
 *
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	
	// A type Random to generate ints used in the createMiddleLetter/createLetterCombination method.
	private static Random ran = new Random();
	
	// Strings to hold the path for text files (English dictionary/letter combinations) for ease of access.
	private static String letterCombinationPath = "src\\datafiles\\letterCombinations.txt";
	private static String englishWordsPath = "src\\datafiles\\english.txt";
	
	// A HashSet<String> to hold all the words held in the English dictionary ("english.txt).
	private static HashSet<String> allWords;
	
	// A HashSet to hold all the possible words that can be created using the given combination of letters
	private HashSet<String> possibleWords = new HashSet<String>();
	
	//String to hold a letter combination pulled from "letterCombinations.txt". 
	private String allLetters;
	// Char to hold the, randomly selected, center letter from allLetters. 
	private char centerLetter;
	
	// A HashSet<String> to hold all correctly guessed words.
	private HashSet<String> wordsFound = new HashSet<String>();
	
	// An int to hold the current player score.
	int playerScore;
	
	// An int[] containing 5 numbers, (25,50,75,90,100)% of the total possible points a player can win given their letterCombination
	private int[] maxPoints = new int[5];
	
	
	/**A constructor that initializes the neccessary fields in the SpellingBeeGame. allLetters being randomly chosen/generated.
	 */
	public SpellingBeeGame(){
		allWords = allWords==null?createWordsFromFile(englishWordsPath):allWords;
		this.playerScore = 0;
		this.allLetters = createLetterCombination();
		this.centerLetter = createMiddleLetter();
		possibleWords = getPossibleWords();
		this.maxPoints = maximumPoints(this.allLetters);
	}
	
	
	/**A constructor that initialized the neccesary fields in the SpellingBeeGame. It takes as input a String representing allLetters.
	 * @param gameLetters 
	 */
	public SpellingBeeGame(String gameLetters){
		
		if(gameLetters.length() == 7) {
			allWords = allWords == null?createWordsFromFile(englishWordsPath):allWords;
			this.playerScore = 0;
			this.allLetters = gameLetters;
			this.centerLetter = createMiddleLetter();
			possibleWords = getPossibleWords();
			this.maxPoints = maximumPoints(this.allLetters);
		}
		else {
			throw new IllegalArgumentException("The input must be 7 letters");
		}
	}
	
	
	/**A method that reads each line from the "english.txt" file in order to subsequently create a HashSet<String> 
	 * containing all of the files contents.
	 * @param path A String containing the path to the letterCombination file.
	 * @return HashSet<String> - The set of combinations
	 */
	public HashSet<String> createWordsFromFile(String path) {
		//Declaring Path/interim List and final returning HashSet
		Path pathToFile = Paths.get(path);
		HashSet<String> returnSet = new HashSet<String>();
		 
		//Reads lines from the txt file and inserts them into the List englishWords
		try {
			
			returnSet.addAll(Files.readAllLines(pathToFile));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("There was a problem reading the english text file");
		}
		
		
		toLowerCase(returnSet);
		return returnSet;
	}
	
	/**A method that stores the contents of the letterCombination file in a List, in order to then randomly pick a line
	 * to serve as the value for allLetters (Combination of laters).
	 * @return String - Letter combination for user
	 * @throws IOException
	 */
	private String createLetterCombination() {
		
		//Declaring Path/interim List
		Path pathToLetters = Paths.get(letterCombinationPath);
		List<String> letterCombinationsL = new ArrayList<String>();
		
		//Reads the txt file and stores all lines in the List
		try {
			letterCombinationsL = Files.readAllLines(pathToLetters);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("There was a problem reading the letterCombination text file");
		}
		
		//Stores one random line into a String to use as the letter combination for the game
		String returnString = letterCombinationsL.get(ran.nextInt(letterCombinationsL.size()));
		return returnString;
		
	}
	
	/**A method that randomly chooses a center letter for the given letterCombo.
	 * @param letterCombo the allLetters for the game
	 * @return char - the center letter as a char
	 */
	private char createMiddleLetter() {
		int middleLetterPosition = ran.nextInt(allLetters.length());
		return allLetters.charAt(middleLetterPosition);
	}
	
	
	
	/** A method that that creates a HashSet containing all the possible words that can be created using the given letter combination 
	 * @param centerLetter The center letter of the combination
	 * @param allLetters The letter combination for the game 
	 * @return HashSet<String> - the HashSet with all possible valid words
	 */
	private HashSet<String> getPossibleWords(){
		

		HashSet<String> returnSet = new HashSet<String>();
		//For each word in the english language, check if it contains the centerLetter and if the word is longer than 4
		for(String word: allWords) {
			if(word.contains(Character.toString(centerLetter)) && word.length() >= 4  ) {
				int count = 0;
				for(int i = 0; i < word.length(); i++) {
					//if it respects the first two  conditions, check if the word contains letters from allLetters, if so, increment count
					if(allLetters.indexOf(word.charAt(i))!= -1) {
						count++;
					}
				}
				// If count is equal to word.length then it should contain letters from the combination, therefore it respects all three conditions
				// Add unto the returnSet 
				if(count == word.length()) {
					returnSet.add(word);
				}
			}
		}
		return returnSet;
	}
	

	/**This method calculates the points to be awarded to the player for the attempt string they input
	 *@param attempt the users word guess
	 *@return int - the number of points awarded to the user
	 */
	@Override
	public int getPointsForWord(String attempt) {
		int attemptLength = attempt.length();
		
		if(possibleWords.contains(attempt)) {
			if(attemptLength == 4) {
				return 1;
			}
			if(attemptLength > 4 && isPanagram(attempt,allLetters)) {
				return attemptLength + 7;
			}
			if(attemptLength > 4) {
				return attemptLength;
			}
		}
		return 0;
	}

	
	/**A helper method to verify whether a given word is a panagram
	 * @param attempt the users word guess
	 * @param allLetters the games letter combination 
	 * @return boolean - true if it is a panagram. False if not.
	 */
	public static boolean isPanagram(String attempt,String allLetters) {
		
		int count = 0;
		//for each letter in the combination, check if it appears in the attempt
		//if so, increment the count. If all letters are present, the count should equal the length of the combination
		for(int i=0; i<allLetters.length();i++) {
			if(attempt.indexOf(allLetters.charAt(i)) != -1) {
				count++;
			}
		}
		//if count is equal to 7(all letters present), then return true, else return false
		return (count == allLetters.length())?true:false;
	}
	


	/**This method calculates the maxmimum points a player can win with their given letter combination and
	 * subsequently populates it with differing point milestones (25,50,75,90,100)%
	 * @param allLetters the combination of letters for the game
	 * @return int[] - the maxpoint bracket array
	 */
	public int[] maximumPoints(String allLetters) {
		
		int maxPoints = 0 ;
		int[] maxPointsBrackets = new int[5];
		// For each possible word, add the appropriate amount of points to the maxPoints variable 
		for(String word: possibleWords) {
			if(word.length() == 4) {
				maxPoints += 1;
			}
			else if(word.length() > 4 && isPanagram(word,allLetters)) {
				maxPoints += word.length() + 7;
			}
			else if(word.length()>4) {
				maxPoints += word.length();
			}
		}
		
		//populates the bracketArray with its repspective milestones
		maxPointsBrackets[0] = (int) (maxPoints * 0.25);
		maxPointsBrackets[1] = (int) (maxPoints * 0.5);
		maxPointsBrackets[2] = (int) (maxPoints * 0.75);
		maxPointsBrackets[3] = (int) (maxPoints * 0.9);
		maxPointsBrackets[4] = maxPoints;
		
		
		return maxPointsBrackets;
	}
	
	/**A helper method that converts every line in the allWords set into lower case
	 * @param returnSet normalized set
	 */
	private static void toLowerCase(HashSet<String> returnSet) {
		//Converting set into String[] to use the built in toLowerCase method
		String[] lowerCaseStrings = returnSet.toArray(new String[returnSet.size()]);
		//Performs the toLowerCase method on all the lines
		for(int i = 0; i < lowerCaseStrings.length; i++) {
			lowerCaseStrings[i] = lowerCaseStrings[i].toLowerCase();
		}
		//Clears the set and adds the normalized lines
		returnSet.clear();
		returnSet.addAll(Arrays.asList(lowerCaseStrings));
	}
	
	/**This method verifies whether or not the attempt is valid and handles the players score accordingly.
	 *@return String - the attempt result String
	 */
	@Override
	public String getMessage(String attempt) {
		attempt = attempt.toLowerCase();
		if(possibleWords.contains(attempt) && !wordsFound.contains(attempt)) {
			int pointsWon = getPointsForWord(attempt);
			this.playerScore += pointsWon;
			wordsFound.add(attempt);
			return "Correct! You've received "+pointsWon+" points!:" + this.playerScore;
		}
		
		else if(wordsFound.contains(attempt)) {
			return "You've already guessed this!:"+this.playerScore;
		}
		
		return "Guessed wrong! You've received 0 points. :"+this.playerScore;
	}

	/**This helper method converts the int array of score brackets to a String in order for 
	 * the action() method return type (in the ServerController) to be respected.
	 * @return String - value of getBrackets
	 */
	public String getBracketsString() {
		String bracketsString= "";
		int[] brackets = this.getBrackets();
		for(int i= 0; i<brackets.length;i++) {
			bracketsString += brackets[i] + " ";
		}
		return bracketsString;
	}
	/**This method should return the set of 7 letters (as a String) that the spelling bee object is storing
	 * @return String - the letter combination
	 */
	@Override
	public String getAllLetters() {
		return this.allLetters;
	}
	/**This method should return the center character. That is, the character that is required to be part of every word.
	 * @return char - center L=letter.
	 */
	@Override
	public char getCenterLetter() {
		return this.centerLetter;
	}
	/**This method should return the current score of the user.
	 * @return int - the current score of the player
	 */
	@Override
	public int getScore() {
		return this.playerScore;
	}
	/**This method will be used in the gui to determine the various point categories.
	 * @return int[] the score achievements.
	 */	
	@Override
	public int[] getBrackets() {
		return this.maxPoints;
	}
	
	
	

}
