package spellingbee.server;

import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;

/**
 * @author Juan-Carlos Sreng-Flores
 * This is a simple spelling bee game. It only uses the same set of words "demyacc" where a possible panagram would be academy --> school appropriate :) 
 * 
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	//Static attributes.
	private static final String DIR = "src\\datafiles\\english.txt"; 
	private static HashSet<String> possibleWords = createWordsFromFile(DIR);
	//instance attributes.
	private char center;
	private int score;
	private String combination;
	private HashSet<String> alreadyFound;
	private int[] pointScheme;
	
	/**
	 * This constructor creates a SimpleSpellingBeeGame Object, where the letter combination is always the same.
	 * @throws IOException
	 */
	public SimpleSpellingBeeGame(){
		if(possibleWords == null) {
			throw new NullPointerException("File "+DIR+" not found. Leaving the possibleWords list empty. Game not playable.");
		}
		this.score = 0;
		this.center = 'e';
		this.combination = "demyaac";
		this.alreadyFound = new HashSet<String>();
		this.pointScheme = new int[5];
		this.pointScheme[4] = 0;
		HashSet<String> validWords = new HashSet<String>();
		for(String word : possibleWords) {
			int points = getPointsForWord(word);
			if(points > 0) {
				pointScheme[4] += points; //calculate maximum score.
			}
		}
		
		//25% maximum score.
		this.pointScheme[0] = (int)(this.pointScheme[4]*0.25);
		//Makes sure the user does not start at 25% if the maximum points is a low number.
		//50% maximum score.
		this.pointScheme[1] = (int)(this.pointScheme[4]*0.5);
		//75% maximum score.
		this.pointScheme[2] = (int)(this.pointScheme[4]*0.75);
		//90% maximum score.
		this.pointScheme[3] = (int)(this.pointScheme[4]*0.9);
	}
	/**
	 * 
	 * This method creates a HashSet of all the words from an english.txt file.
	 * @param file - String url path to the english words file.
	 * @return HashSet<String> returns a HashSet where it holds all of the possible words in english, all lowercased.
	 * If the file is not found, it returns null. 
	 * 
	 */
	private static HashSet<String> createWordsFromFile(String file) {
		Path path = Paths.get(file);
		HashSet<String> words;
		try {
			List<String> list = Files.readAllLines(path);
			//Prepare HashSet size so it doesn't have to resize too often.
			words = new HashSet<String>(list.size());
			//add the word to LowerCase.
			for(String line : list) {
				words.add(line.toLowerCase());
			}
		}
		catch(IOException e) {
			words = null;
		}
		return words;
	}
	/**
	 * This method uses a String and evaluates how many points the guess word will the user receive. 
	 * - If the letter is less than 4 letters, no points are received.
	 * - If all the letters are contained in the combination of letters, found in the dictionary, and not already found by the user,
	 * the user gets a minimum of a full word length worth of points if the guess word in > 4. If guess.length() == 4, the user gets 1 point.
	 * If the user manages to use all of the letters of the combination, she/he gets 7 extra bonus points.
	 * @param guess - A String that the user guesses, and is to be tested for points.
	 * @return int - the number of points the user gets for an a guess.
	 */
	@Override
	public int getPointsForWord(String guess) {
		guess = guess.toLowerCase();
		int points = 0;
		int guessLength = guess.length();
		//If the letter is found 
		if(guessLength >= 4) {
			boolean isValid = true;
			//Check if all the letters in attempt are found in the hand.
			isValid = Utility.containsAllLetters(guess, this.combination);
			//Check if the user guess is found in the dictionary. Also check if the guess has not already been made before
			isValid = isValid && !this.alreadyFound.contains(guess) && guess.contains(""+center) && possibleWords.contains(guess);
			if(isValid) {
				//If the length is 4, the user gets only 1 point for its guess.
				if(guessLength == 4) {
					points = 1;
				}
				//else guess is already >= 4, and != 4 so the user gets full length worth of points.
				else {
					points = guessLength;
					//Adds 7 bonus points if all letters are used. 
					if(guessLength >= 7 && Utility.containsAllLetters(this.combination, guess)) {
						points += 7;
					}
				}
			}
		}
		return points;
	}
	
	/**This method should check if the word attempt is a valid word or not according to the Spelling Bee rules. 
	 * It should return a message based on the reason it is rejected or a positive message (e.g. �good� or �great�) if it is a valid word.
	 * @param attempt
	 * @return String - A message 
	 */
	@Override
	public String getMessage(String attempt) {
		attempt = attempt.toLowerCase();
		int point = getPointsForWord(attempt);
		this.score += point;
		String message = "You received "+point+" point"+(point==1?"":"s");
		if(point == attempt.length()+7) {
			message ="Amazing! "+"You received "+(point-7)+" points along with 7 bonus points!:"+this.score;
			this.alreadyFound.add(attempt);
		}
		else if(point > 0 ) {
			message =  (point>1?"Good job! "+message+".":"Great! "+message+".")+":"+this.score;
			this.alreadyFound.add(attempt);
		}
		else {
			if(alreadyFound.contains(attempt))
				message="You've already found this word!";
			else {
				message =  message+".";
			}
			message = message+" Try again!:"+this.score;
		}
		return message;
	}
	
	/**This method should return the set of 7 letters (as a String) that the spelling bee object is storing
	 * @return String - the combination of letter (the hand).
	 */
	@Override
	public String getAllLetters() {
		return this.combination;
	}
	/**This method should return the center character. That is, the character that is required to be part of every word.
	 * @return char - the center character.
	 */
	@Override
	public char getCenterLetter() {
		return center;
	}
	
	/**This method should return the current score of the user.
	 * @return int - the score the user currently has.
	 */
	@Override
	public int getScore() {
		return score;
	}

	/**This method will be used in the gui to determine the various point categories.
	 * This method clones the point scheme for scoping purposes and encapsulation.
	 * @return int[] - the cloned pointScheme for the point Scheme of this game.
	 */
	@Override
	public int[] getBrackets() {
		int[] clone  = new int[pointScheme.length];
		for(int i = 0; i<clone.length; i++) {
			clone[i] = pointScheme[i];
		}
		return pointScheme;
	}
	
	
}
