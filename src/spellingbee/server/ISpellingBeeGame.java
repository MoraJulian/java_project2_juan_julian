package spellingbee.server;

/**
 * @author Juan Carlos Sreng-Flores , Julian Mora
 *
 */
public interface ISpellingBeeGame {
	
	
	/**This method should return the number of points that a given word is worth according to the Spelling Bee rules
	 * @param attempt
	 * @return String
	 */
	int getPointsForWord(String attempt);
	
	/**This method should check if the word attempt is a valid word or not according to the Spelling Bee rules. 
	 * It should return a message based on the reason it is rejected or a positive message (e.g. �good� or �great�) if it is a valid word.
	 * @param attempt
	 * @return String
	 */
	String getMessage(String attempt);
	
	/**This method should return the set of 7 letters (as a String) that the spelling bee object is storing
	 * @return String
	 */
	String getAllLetters();
	
	/**This method should return the center character. That is, the character that is required to be part of every word.
	 * @return char
	 */
	char getCenterLetter();
	
	/**This method should return the current score of the user.
	 * @return int
	 */
	int getScore();
	
	/**This method will be used in the gui to determine the various point categories.
	 * @return int[]
	 */
	int[] getBrackets();
	
	
	
}
