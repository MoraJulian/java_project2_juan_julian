package spellingbee.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import spellingbee.server.SpellingBeeGame;

class SpellingBeeGameTest {
	private SpellingBeeGame test = new SpellingBeeGame();
	private SpellingBeeGame customCombination = new SpellingBeeGame("atfqclr");
	
	
	@Test
	void testCustomAllLetters() {
		
		String expectedOutput = "The input must be 7 letters";
		
		try{
			SpellingBeeGame customtest = new SpellingBeeGame("kk");
		}
		catch(IllegalArgumentException e) {
			assertEquals(expectedOutput,e.getMessage());
		}
		
	}
	
	@Test
	void testCreateLetterCombinationCustom() {
		//And by proxy it also tests getAllLetters()
		String expectedOutput = "atfqclr";
		assertEquals(expectedOutput,customCombination.getAllLetters());
		
	}
	
	
	@Test
	void testCreateLetterCombination() {
		//TODO Find an actual way to test this 
		String expectedOutput = test.getAllLetters();
		
		assertEquals(expectedOutput,test.getAllLetters());
		
	}
	
	@Test
	void testGetCenterLetter() {
		
		char expectedOutput = test.getCenterLetter();
		System.out.println(expectedOutput);
		
		assertEquals(expectedOutput,test.getCenterLetter());
		
	}

}
