package spellingbee.test;

import static org.junit.jupiter.api.Assertions.*;
import spellingbee.server.Utility;
import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;

import org.junit.jupiter.api.Test;

class UtilityTest {

	/**
	 * This test tests for the Utility class Method where it compares two strings, and see if all the 
	 * letters of the first string are contained in the second string.
	 */
	@Test
	void testContainsAllLetters() {
		assertEquals(true,Utility.containsAllLetters("abc", "abcde"));
		assertEquals(false, Utility.containsAllLetters("abcde", "abc"));
		//duplicate letters
		assertEquals(true, Utility.containsAllLetters("demyacccccccccc","academy"));
		
	}
	

}
