package spellingbee.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import spellingbee.server.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * This class is a Tester for the SimpleSpellingBeeGame class.
 * It tests all of its public method.
 * @author Juan-Carlos Sreng-Flores
 */
class SimpleSpellingBeeGameTest {

	/**
	 * Test the SimpleSpellingBeeGame constructor, if the method throws an Exception, constructor fails
	 * the test, or the file is not at the right directory.
	 */
	@Test
	void testConstructor() {
		try {
			SimpleSpellingBeeGame test = new SimpleSpellingBeeGame();
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	/**
	 * Test the getPointsForWord. 
	 * If the words does not follow the valid word restrictions the method should return 0.
	 * 
	 */
	@Test
	void testGetPointsForWord() {
		try {
			SimpleSpellingBeeGame test = new SimpleSpellingBeeGame();
			//Should fail.
			//Test empty string.
			assertEquals(0, test.getPointsForWord(""));
			//Test non existing word.
			assertEquals(0, test.getPointsForWord("aacdemy"));
			//Test word length 3.
			assertEquals(0,test.getPointsForWord("dye"));
			//Test word length 2.
			assertEquals(0,test.getPointsForWord("de"));
			//Test word without center letter
			assertEquals(0, test.getPointsForWord("dama")); //existing word without center letter e.
			//Should pass.
			//panagram
			assertEquals(14,test.getPointsForWord("academy"));
			//Non panagram
			assertEquals(7,test.getPointsForWord("academe"));
			assertEquals(5,test.getPointsForWord("adeem"));
			//Length 4
			assertEquals(1,test.getPointsForWord("cede"));
			assertEquals(1,test.getPointsForWord("came"));
			assertEquals(1,test.getPointsForWord("dame"));
		}
		catch(Exception e ) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * This method tests if the getMessage method works properly, the method needs to add the points to the score
	 * if the user wins points or not. It tests if the appropriate messages are returned properly/
	 */
	@Test
	void testGetMessage() {
		try {
			SimpleSpellingBeeGame test = new SimpleSpellingBeeGame();
			assertEquals("Amazing! You received 7 points along with 7 bonus points!:14", test.getMessage("academy"));
			assertEquals(14, test.getScore());
			assertEquals("You received 0 points. Try again!:14",test.getMessage("aacdemy"));
			assertEquals(14, test.getScore());
			assertEquals("Great! You received 1 point.:15", test.getMessage("dame"));
			assertEquals(15, test.getScore());
			assertEquals("Good job! You received 5 points.:20", test.getMessage("adeem"));
			assertEquals(20, test.getScore());
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	/**
	 * Tests if the getter method works properly.
	 */
	@Test
	void testGetAllLetters() {
		try {
			SimpleSpellingBeeGame test = new SimpleSpellingBeeGame();
			assertEquals("demyaac", test.getAllLetters());
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	/**
	 * Tests if getter method works as expected and returns the right character.
	 */
	@Test
	void testGetCenterLetter() {
		try {
			SimpleSpellingBeeGame test = new SimpleSpellingBeeGame();
			assertEquals('e', test.getCenterLetter());
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	/**
	 * Test if the getScore returns the right number, as the game is played.
	 */
	@Test
	void testGetScore() {
		try {
			SimpleSpellingBeeGame test = new SimpleSpellingBeeGame();
			test.getMessage("academy");
			assertEquals(14, test.getScore());
			test.getMessage("aacdemy");
			assertEquals(14, test.getScore());
			test.getMessage("dame");
			assertEquals(15, test.getScore());
			test.getMessage("adeem");
			assertEquals(20, test.getScore());
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	/**
	 * Tests if the getBrackets returns the right point Scheme. 
	 */
	@Test
	void testGetBrackets() {
		try {
			SimpleSpellingBeeGame test = new SimpleSpellingBeeGame();
			int[] pointScheme = test.getBrackets();
			assertEquals(56, pointScheme[0]);
			assertEquals(113, pointScheme[1]);
			assertEquals(170, pointScheme[2]);
			assertEquals(204, pointScheme[3]);
			assertEquals(227, pointScheme[4]);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
}
