package spellingbee.client;

import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.*;

/**
 * This class is a ScoreTab object where it extends the Tab Object. It is used to create
 * a Tab Pane in the SpellingBeeGame to show the progress of the user throughout the game.
 * @author Julian Mora
 * @author Juan-Carlos Sreng-Flores
 *
 */
public class ScoreTab extends Tab {
	private Client client;
	private Text[] categoryCol = new Text[5];
	private Text[] valueCol = new Text[5];
	private Text playerScorePoints = new Text("0");
	private int[] brackets;
	
	
	
	/**
	 * This constructor creates a tab pane where the score will be displayed, and the progress will be shown
	 * to the user.
	 * @param client
	 */
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		this.setContent(setUpGrid());
	}
	/**
	 * This method sets up the grid pane to hold all of the Nodes in the Score Tab.
	 * @return GridPane - The GridPane to display the levels achieved by the user.
	 */
	public GridPane setUpGrid() {
		
		String[] bracketsString = this.client.sendAndWaitMessage("getBrackets").split(" ");
		brackets = new int[bracketsString.length];
		for(int i = 0; i<bracketsString.length; i++) {
			brackets[i] = Integer.parseInt(bracketsString[i]);
		}
		
		
		categoryCol[0] = new Text("Queen Bee");
		categoryCol[1] = new Text("Genius");
		categoryCol[2] = new Text("Amazing");
		categoryCol[3] = new Text("Good");
		categoryCol[4] = new Text("Getting Started");
		Text playerScore = new Text("Current Score");
		
		for(int i = 0; i<brackets.length; i++) {
			valueCol[i] = new Text(""+brackets[(brackets.length-1)-i]);
		}
		this.playerScorePoints = new Text("0");
		
		GridPane pane = new GridPane();
		
		for(int i  = 0; i<categoryCol.length;i++) {
			categoryCol[i].setFill(Color.GREY);
			pane.add(categoryCol[i], 0, i);
		}
		
		for(int j = 0; j<valueCol.length;j++) {
			valueCol[j].setFill(Color.GREY);
			pane.add(valueCol[j], 1, j);
		}
			
		pane.setHgap(10);
		
		playerScore.setFill(Color.GREEN);
		playerScorePoints.setFill(Color.GREEN);
		pane.add(playerScore,0,5);
		pane.add(playerScorePoints,1,5);
		return pane;
		
	}
	/**
	 * This method refreshes the score of the user as the game goes, whenever there's new word to submit,
	 * it requests the score and restore the scoreboard. 
	 */
	public void refresh() {
		int currentScore = Integer.parseInt(this.client.sendAndWaitMessage("getScore"));
		this.playerScorePoints.setText(Integer.toString(currentScore));
		for(int i = 0; i<brackets.length; i++) {
			if(currentScore >= brackets[i]) {
				valueCol[brackets.length-1-i].setFill(Color.GREEN);
				categoryCol[brackets.length-1-i].setFill(Color.GREEN);
			}
			else {
				break;
			}
		}
	}
	
	
}
