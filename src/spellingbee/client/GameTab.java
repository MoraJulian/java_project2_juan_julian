package spellingbee.client;


import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import spellingbee.network.Client;
import javafx.event.ActionEvent;

/**
 * @author Juan-Carlos Sreng-Flores
 * Tab class where all the necessary parts of it will be used to construct the game tab GUI in the game.
 *
 */
public class GameTab extends Tab{
	private ScoreTab scoreTab;
	private String combination;
	private char center;
	private Button[] letterButtons = new Button[7];
	private TextField userInput;
	private Button submit;
	private Button clear;
	private Text message;
	private Text score;
	private Client client;
	
	/**
	 * This constructor builds the GameTab Object that extends Tab from JavaFx in the GUI application. 
	 * @param client
	 * @param scoreTab ScoreTab Object where the score tab gets updated whenever a new submit was sent.
	 */
	public GameTab(Client client, ScoreTab scoreTab) {
		super("Game");
		this.scoreTab = scoreTab;
		this.client = client;
		this.combination = client.sendAndWaitMessage("getLetters");
		//check if received the right amount of letters.
		if(this.combination.length() != 7) {
			throw new IllegalArgumentException("Combination of letter received is invalid. Was expecting 7 letters but received "+this.combination.length()+" letters");
		}
		String center = client.sendAndWaitMessage("getCenter");
		//check if received the right amount of letters for the center letter.
		if(center.length() == 1) {
			this.center = center.charAt(0);
		}
		else {
			throw new IllegalArgumentException("Center letter is invalid. Could not start the game. Was expecting 1 letter but received: "+center);
		}
		setupFrameWork();
		setupEventListener();
	}
	
	/**
	 * This method is a helper method to initialize all the essential buttons and textfields in order to interact
	 * with the GUI. It returns the last VBox to be inserted into the GameTab pane. 
	 */
	private void setupFrameWork() {
		//Adding buttons and adding center letter with color. 
		HBox horizontalButtons = new HBox();
		horizontalButtons.setSpacing(5);
		boolean centerLetterColored = false;
		for(int i = 0; i<this.combination.length(); i++) {
			char letter = this.combination.charAt(i);
			if(!centerLetterColored && letter == this.center) {
				letterButtons[i] = new Button(""+letter);
				horizontalButtons.getChildren().add(letterButtons[i]);
				letterButtons[i].setStyle("-fx-background-color: #ffcc00");
				centerLetterColored = true;
			}
			else {
				letterButtons[i] = new Button(""+letter);
				horizontalButtons.getChildren().add(letterButtons[i]);
			}
			letterButtons[i].setScaleX(1.2);
			letterButtons[i].setScaleY(1.2);
		}
		
		//Creating input textfield for user.
		userInput = new TextField();
		//Creating submit button
		submit = new Button("Submit");
		//
		clear = new Button("Clear");
		//Creating message text
		message = new Text("Go ahead and play!");
		//Creating score text
		int points = Integer.parseInt(client.sendAndWaitMessage("getScore"));
		score = new Text("Your total score: "+points+(points==1?" point.":" points."));
		//Create FrameWork.
		//Message from server and the score HBox
		VBox messageAndScore = new VBox();
		messageAndScore.setSpacing(15);
		messageAndScore.getChildren().addAll(message, score);
		//Submit and clear button HBox
		HBox submitAndClear = new HBox();
		submitAndClear.setSpacing(15);
		submitAndClear.getChildren().addAll(submit, clear);
		VBox buttonsAndInputText = new VBox();
		buttonsAndInputText.getChildren().addAll(horizontalButtons, userInput);
		VBox messageScoreAndSubmitClear = new VBox();
		messageScoreAndSubmitClear.getChildren().addAll(submitAndClear, messageAndScore);
		VBox lastVBox = new VBox();
		lastVBox.getChildren().addAll(buttonsAndInputText, messageScoreAndSubmitClear);
		this.setContent(lastVBox);
	}
	
	/**
	 * This method is a helper method to initialize all the essential EventListener of the buttons and 
	 * textfields.
	 */
	private void setupEventListener() {
		//Setup buttons click event, where it will insert the letter into the userInput.
		for(Button button : letterButtons) {
			button.setOnAction(new EventHandler<ActionEvent>() {
				/**
				 * This method is for all the buttons of the game. It insert it's letter inside the
				 * user input whenever the button is clicked.
				 * @param ActionEvent e
				 */
				@Override
				public void handle(ActionEvent e) {
					userInput.setText(userInput.getText()+button.getText());
				}
			});
		}
		//Setup submit button's event handler
		submit.setOnAction(new EventHandler<ActionEvent>() {
			/**
			 * This method is for all the submit button of the game. It will submit the word in 
			 * the user input TextField to the server where it will be processed and updated in the 
			 * GUI.
			 * @param ActionEvent e
			 */
			@Override
			public void handle(ActionEvent e) {
				String text = userInput.getText();
				if(!text.equals("")) {
					String response = client.sendAndWaitMessage("getMessage:"+text);
					if(!response.equals("No change")) {
						String[] messageAndScore = response.split(":");
						if(messageAndScore.length == 2) {
							message.setText(messageAndScore[0]);
							score.setText("Your total score: "+messageAndScore[1]);
							userInput.setText("");
							scoreTab.refresh();
						}
						
					}
				}
			}
		});
		//Setup clear button's event handler
		clear.setOnAction(new EventHandler<ActionEvent>() {
			/**
			 * This method is simply used to remove any letters or words written in the user input
			 * TextField GUI. 
			 * @param ActionEvent e
			 */
			@Override
			public void handle(ActionEvent e) {
				userInput.setText("");
			}
		});
	}
}
