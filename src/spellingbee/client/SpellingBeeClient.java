package spellingbee.client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.*;

/**
 * 
 * @author Julian Mora
 * @author Juan-Carlos Sreng-Flores
 * This class is the Application launcher of the SpellingBeeGame. It will prompt a graphical user interface in order 
 * to play the game.
 */
public class SpellingBeeClient extends Application {
	
	private Client client = new Client();
	
	/**
	 * @author Julian Mora
	 * @author Juan-Carlos Sreng-Flores
	 * This method is used to start the application launching. 
	 * @param stage - Stage parameter, used with Application.launch(args).
	 */
	public void start(Stage stage) {
		
		//Container Declarations
		Group root = new Group();
		
		//Create the TabPane size.
		TabPane tp = new TabPane();
		tp.setMinHeight(300);
		tp.setMinWidth(650);
		ScoreTab score = new ScoreTab(client);
		GameTab game = new GameTab(client, score);
		tp.getTabs().addAll(score,game);
		
		
		root.getChildren().add(tp);		
		

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);

		//associate scene to stage and show
		stage.setTitle("SpellingBeeClient");
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }

}
